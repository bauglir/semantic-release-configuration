/**
 * Determines the type of branch for the provided `branch`, i.e. whether it
 * represents a "maintenance" or a "prerelease" branch.
 *
 * @return object The `branch_prefix` and whether it `is_prerelease`.
 */
function determinePrefixedBranchType(
  branch,
  { maintenance_branch_prefix, prerelease_branch_prefix },
) {
  if (branch.startsWith(maintenance_branch_prefix)) {
    return { branch_prefix: maintenance_branch_prefix, is_prerelease: false };
  } else {
    return { branch_prefix: prerelease_branch_prefix, is_prerelease: true };
  }
}

/**
 * Determines the "branches" for a semantic-release configuration given the
 * provided `configuration` for the shareable configuration. This ensures more
 * (patterned) release branches can be supported than would typically be the
 * case.
 *
 * The function is capable of dealing with "primary release branches", the
 * corresponding "prerelease branch", and patterned "maintenance" and
 * "prerelease" branches.
 *
 * @return array An array containing the determined branch configuration as the
 * sole entry.
 */
export function BranchesConfiguration(branch, configuration) {
  if (configuration.primary_release_branch === branch) {
    return [branch];
  }

  if (configuration.prerelease_branch_prefix === branch) {
    return [
      configuration.primary_release_branch,
      { name: branch, prerelease: configuration.prerelease_tag_suffix },
    ];
  }

  const { branch_prefix, is_prerelease } = determinePrefixedBranchType(
    branch,
    configuration,
  );

  // The matching pattern is configured to match all allowed permutations of
  // version specifications as closely as possible to ensure other permutations
  // that look valid, e.g. `N.z.q`, do not also accidentally match
  const branch_prefix_separator = configuration.branch_prefix_separator ?? "-";
  const version_branch_matcher = new RegExp(
    `^${branch_prefix}${branch_prefix_separator}(?<major>\\d+)(?<minor>\\.(\\d+|x|y))?(\\.(x|z))?$`,
  );
  if (version_branch_matcher.test(branch)) {
    let {
      groups: { major, minor },
    } = branch.match(version_branch_matcher);

    // Remove minor parts not representing digits, so they are not included in
    // the returned `range`
    const version_part_matcher = new RegExp("\\.\\d+");
    if (!version_part_matcher.test(minor)) {
      minor = "";
    }

    return [
      configuration.primary_release_branch,
      {
        name: branch,
        ...(is_prerelease
          ? { prerelease: configuration.prerelease_tag_suffix }
          : {}),
        range: `${major}${minor}.x`,
      },
    ];
  }

  return [];
}
