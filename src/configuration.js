import { accessSync } from "node:fs";
import { cosmiconfigSync } from "cosmiconfig";
import { env } from "node:process";

/**
 * Helper function to synchronously check whether a file exists on disk, e.g.
 * used to check for default files that should be removed from lists if they
 * are not present.
 */
function fileExists(filename) {
  try {
    accessSync(filename);
    return true;
  } catch {
    return false;
  }
}

const CONFIGURATION_DEFAULTS = {
  // The character(s) to use to separate the prefix for maintenance and
  // prerelease branches from the version pattern
  branch_prefix_separator: env.BRANCH_PREFIX_SEPARATOR || "-",
  changelog_filename: "CHANGELOG.md",
  // Files that may contain versioned templates which will be updated upon
  // release. These are conditionally included to ensure downstream operations
  // to not have to guard against files in this list not being present, e.g.
  // when staging for commits
  files_with_versioned_templates: ["README.md"].filter(fileExists),
  // The branch prefix for patterned maintenance branches
  maintenance_branch_prefix: env.MAINTENANCE_BRANCH_PREFIX || "release",
  // The branch prefix for patterned prerelease branches
  prerelease_branch_prefix: env.PRERELEASE_BRANCH_PREFIX || "next",
  // The tag suffix to use for tags created from prerelease branches
  prerelease_tag_suffix: env.PRERELEASE_TAG_SUFFIX || "rc",
  // The `PRIMARY_RELEASE_BRANCH` environment variable is used as a platform
  // independent mechanism for specifying the primary release branch. On GitLab
  // this defaults to the "default" branch. Given that the "default" branch is
  // not readily available on GitHub `main` is used as a fallback
  primary_release_branch:
    env.PRIMARY_RELEASE_BRANCH || env.CI_DEFAULT_BRANCH || "main",
};

const ethimaConfig = cosmiconfigSync("ethima").search();

// The configuration only supports merging elements at the root object. This is
// sufficient for the current configuration needs, which tend to be simple
// values and doing a deep merge would add currently unnecessary complexity.
// This can be revisited once the need arises
export const CONFIGURATION = {
  ...CONFIGURATION_DEFAULTS,
  ...ethimaConfig?.config,
};
