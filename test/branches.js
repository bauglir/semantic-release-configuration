import { describe, it } from "node:test";

import { BranchesConfiguration } from "../src/branches.js";

function testBranchesConfiguration({ branch, configuration }, expected) {
  return (t) =>
    t.assert.deepEqual(BranchesConfiguration(branch, configuration), expected);
}

describe("BranchesConfiguration", () => {
  it(
    `returns the 'primary release branch' if the branch matches the configuration exactly.`,
    testBranchesConfiguration(
      { branch: "main", configuration: { primary_release_branch: "main" } },
      ["main"],
    ),
  );

  it(
    "returns empty branches when the 'primary release branch' does not match the configuration.",
    testBranchesConfiguration(
      { branch: "not-main", configuration: { primary_release_branch: "main" } },
      [],
    ),
  );

  it(
    "returns the 'main prerelease' branch if it matches the configuration exactly.",
    testBranchesConfiguration(
      {
        branch: "next",
        configuration: {
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "beta",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "next", prerelease: "beta" }],
    ),
  );

  it(
    "returns a 'major prerelease branch (-N)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "next-1",
        configuration: {
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "beta",
          primary_release_branch: "master",
        },
      },
      ["master", { name: "next-1", prerelease: "beta", range: "1.x" }],
    ),
  );

  it(
    "returns empty branches when the 'prerelease branch' does not match the configuration.",
    testBranchesConfiguration(
      {
        branch: "not-release-1",
        configuration: { maintenance_branch_prefix: "release" },
      },
      [],
    ),
  );

  it(
    "returns a 'major prerelease branch (-N.x)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "next-2.x",
        configuration: {
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "rc",
          primary_release_branch: "trunk",
        },
      },
      ["trunk", { name: "next-2.x", prerelease: "rc", range: "2.x" }],
    ),
  );

  it(
    "returns a 'major prerelease branch (-N.x.x)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "prerelease-3.x.x",
        configuration: {
          prerelease_branch_prefix: "prerelease",
          prerelease_tag_suffix: "alpha",
          primary_release_branch: "trunk",
        },
      },
      [
        "trunk",
        { name: "prerelease-3.x.x", prerelease: "alpha", range: "3.x" },
      ],
    ),
  );

  it(
    "returns a 'major prerelease branch (-N.y)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "next-4.y",
        configuration: {
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "rc",
          primary_release_branch: "master",
        },
      },
      ["master", { name: "next-4.y", prerelease: "rc", range: "4.x" }],
    ),
  );

  it(
    "returns a 'major prerelease branch (-N.y.z)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "next-1.y.z",
        configuration: {
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "foo",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "next-1.y.z", prerelease: "foo", range: "1.x" }],
    ),
  );

  it(
    "returns a 'minor prerelease branch' (-N.N) if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "prerelease-2.3",
        configuration: {
          prerelease_branch_prefix: "prerelease",
          prerelease_tag_suffix: "rc",
          primary_release_branch: "foobar",
        },
      },
      ["foobar", { name: "prerelease-2.3", prerelease: "rc", range: "2.3.x" }],
    ),
  );

  it(
    "returns a 'minor prerelease branch' (-N.N.x) if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "next-3.4.x",
        configuration: {
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "alpha",
          primary_release_branch: "trunk",
        },
      },
      ["trunk", { name: "next-3.4.x", prerelease: "alpha", range: "3.4.x" }],
    ),
  );

  it(
    "returns a 'minor prerelease branch' (-N.N.z) if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "next-11.22.z",
        configuration: {
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "beta",
          primary_release_branch: "trunk",
        },
      },
      ["trunk", { name: "next-11.22.z", prerelease: "beta", range: "11.22.x" }],
    ),
  );

  it(
    "returns a 'major maintenance branch (-N)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-1",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "release-1", range: "1.x" }],
    ),
  );

  it(
    "returns empty branches when the 'release branch' does not match the configuration.",
    testBranchesConfiguration(
      {
        branch: "not-release-1",
        configuration: { maintenance_branch_prefix: "release" },
      },
      [],
    ),
  );

  it(
    "returns a 'major maintenance branch (-N.x)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-2.x",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "release-2.x", range: "2.x" }],
    ),
  );

  it(
    "returns a 'major maintenance branch (-N.x.x)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-3.x.x",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "release-3.x.x", range: "3.x" }],
    ),
  );

  it(
    "returns a 'major maintenance branch (-N.y)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-4.y",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "foo",
        },
      },
      ["foo", { name: "release-4.y", range: "4.x" }],
    ),
  );

  it(
    "returns a 'major maintenance branch (-N.y.z)' if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-1.y.z",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "release-1.y.z", range: "1.x" }],
    ),
  );

  it(
    "returns a 'minor maintenance branch' (-N.N) if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-2.3",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "master",
        },
      },
      ["master", { name: "release-2.3", range: "2.3.x" }],
    ),
  );

  it(
    "returns a 'minor maintenance branch' (-N.N.x) if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-3.4.x",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "trunk",
        },
      },
      ["trunk", { name: "release-3.4.x", range: "3.4.x" }],
    ),
  );

  it(
    "returns a 'minor maintenance branch' (-N.N.z) if it matches the configuration.",
    testBranchesConfiguration(
      {
        branch: "release-11.22.z",
        configuration: {
          maintenance_branch_prefix: "release",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "release-11.22.z", range: "11.22.x" }],
    ),
  );

  it(
    "Respect a configured 'branch prefix separator' for maintenance branches.",
    testBranchesConfiguration(
      {
        branch: "release-1.2.x",
        configuration: {
          branch_prefix_separator: "-",
          maintenance_branch_prefix: "release",
          primary_release_branch: "main",
        },
      },
      ["main", { name: "release-1.2.x", range: "1.2.x" }],
    ),
  );

  it(
    "Respect a configured 'branch prefix separator' for prerelease branches.",
    testBranchesConfiguration(
      {
        branch: "next/4.5.z",
        configuration: {
          branch_prefix_separator: "/",
          prerelease_branch_prefix: "next",
          prerelease_tag_suffix: "rc",
          primary_release_branch: "trunk",
        },
      },
      ["trunk", { name: "next/4.5.z", prerelease: "rc", range: "4.5.x" }],
    ),
  );
});
