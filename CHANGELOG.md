# Changelog

## [9.0.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v9.0.0...v9.0.1) (2024-11-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/github to v11.0.1 ([3ecc8b3](https://gitlab.com/ethima/semantic-release-configuration/commit/3ecc8b389aec1dd76e2eca0bb99b544a3480be47))

# [9.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v8.0.1...v9.0.0) (2024-10-29)


* build(deps)!: update dependency @semantic-release/github to v11 ([56e297b](https://gitlab.com/ethima/semantic-release-configuration/commit/56e297b62c71462fc5e6ee502ae3a5754d8e630b))


### BREAKING CHANGES

* the minimum required version of `semantic-release` to
use this configuration is now v24.1.0 due to requirements imposed by
`@semantic-release/github@11.0.0`
(semantic-release/github@95c7cdd19b9f053913839cf7b0f9a55bd8f0e494).

## [8.0.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v8.0.0...v8.0.1) (2024-02-15)


### Bug Fixes

* **versioned-templates:** include "default files" only when actually present ([f6908d5](https://gitlab.com/ethima/semantic-release-configuration/commit/f6908d5262f4d86b7ab2674087dd7b5b483fa4e2))

# [8.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v7.0.0...v8.0.0) (2024-02-15)


### Build System

* **deps:** update dependency cosmiconfig to v9 ([b99d495](https://gitlab.com/ethima/semantic-release-configuration/commit/b99d4956864e03c62d398b85b8c5459f2c6d6f69))


### BREAKING CHANGES

* **deps:** if a project is using a `config.js` file, or an
equivalent such as `config.json` or `config.ts`, to _configure
`cosmiconfig` itself_, the file needs to be moved into a `.config`
directory in the root of the project.

See `cosmiconfig`'s ["Usage for end users"
documentation][cosmiconfig-meta-config-url] for details and the [v9.0.0
release notes][cosmiconfig-v9-release-notes-url].

[cosmiconfig-meta-config-url]: https://github.com/cosmiconfig/cosmiconfig/blob/a5a842547c13392ebb89a485b9e56d9f37e3cbd3/README.md#usage-for-end-users
[cosmiconfig-v9-release-notes-url]: https://github.com/cosmiconfig/cosmiconfig/releases/tag/v9.0.0

# [7.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v6.0.0...v7.0.0) (2023-12-27)


### Code Refactoring

* migrate to ES modules ([64acf9b](https://gitlab.com/ethima/semantic-release-configuration/commit/64acf9b97c8f38f8c90340c41fc866deb9fda278))


### Features

* commit `package-lock.json` files on release when tracked ([47461d5](https://gitlab.com/ethima/semantic-release-configuration/commit/47461d59f4ec02ab00b2cc910a664230e1acc8fb))


### BREAKING CHANGES

* The configuration has been rewritten to use ES modules.
This requires using at least `semantic-release@22.0.7` which added
support for loading shareable configurations implemented as ES modules.
This is declared as a "peer dependency" and therefore will only result
in warnings (and eventually an error) when used with an incompatible
version.

# [7.0.0-rc.2](https://gitlab.com/ethima/semantic-release-configuration/compare/v7.0.0-rc.1...v7.0.0-rc.2) (2023-12-27)


### Features

* commit `package-lock.json` files on release when tracked ([47461d5](https://gitlab.com/ethima/semantic-release-configuration/commit/47461d59f4ec02ab00b2cc910a664230e1acc8fb))

# [7.0.0-rc.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v6.0.0...v7.0.0-rc.1) (2023-12-27)


### Code Refactoring

* migrate to ES modules ([64acf9b](https://gitlab.com/ethima/semantic-release-configuration/commit/64acf9b97c8f38f8c90340c41fc866deb9fda278))


### BREAKING CHANGES

* The configuration has been rewritten to use ES modules.
This requires using at least `semantic-release@22.0.7` which added
support for loading shareable configurations implemented as ES modules.
This is declared as a "peer dependency" and therefore will only result
in warnings (and eventually an error) when used with an incompatible
version.

# [6.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v5.0.0...v6.0.0) (2023-12-25)


* feat!: define the minimum required Node.js version explicitly ([cb057e4](https://gitlab.com/ethima/semantic-release-configuration/commit/cb057e4aaf310aa3f3c6cc56eee85f16ba5bdfab))


### BREAKING CHANGES

* The minimum Node.js version required to use the
configuration has been incremented to v20.8.1. This is primarily to
facilitate an update of `@semantic-release/gitlab`[^1] to v13, see also
!83.

This primarily impacts use of the configuration when deploying to
GitLab. When deploying to GitHub this release may be considered a
non-breaking release given the `engines` field[^2] is advisory and only
results in warnings. The presence of the field only results in errors if
a user explicitly opts in to the `engine-strict` configuration[^3] of
NPM.

[^1]: https://github.com/semantic-release/gitlab
[^2]: https://docs.npmjs.com/cli/v10/configuring-npm/package-json#engines
[^3]: https://docs.npmjs.com/cli/v10/using-npm/config#engine-strict

# [5.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v4.1.2...v5.0.0) (2023-12-17)

* feat!: use the `build` type for release commits ([92ed242](https://gitlab.com/ethima/semantic-release-configuration/commit/92ed24244b1c87e2b57e52c12fb2ad07ca5f5341))

### BREAKING CHANGES

* Although the `chore` type is valid in the context of conventional
  commits, it has been dropped from the Angular commit conventions as it is
  too generic. The Ethima organization has adopted the suggestion of adopting
  the `build` type for these commits instead.

  This is considered a breaking change as some may be relying on the previous
  type for automation, etc.

## [4.1.2](https://gitlab.com/ethima/semantic-release-configuration/compare/v4.1.1...v4.1.2) (2023-10-24)

### Bug Fixes

* **deps:** update dependency conventional-changelog-conventionalcommits to v7 ([536015f](https://gitlab.com/ethima/semantic-release-configuration/commit/536015f5ad9c553118782314cca30c85978258db)), closes [/gitlab.com/ethima/semantic-release-configuration/-/blob/76a150478f1e60655c45479ad20af69e96dba220/src/index.js#L82](https://gitlab.com//gitlab.com/ethima/semantic-release-configuration/-/blob/76a150478f1e60655c45479ad20af69e96dba220/src/index.js/issues/L82)

## [4.1.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v4.1.0...v4.1.1) (2023-07-15)


### Bug Fixes

* add the branch configuration file to the NPM package ([a18d67f](https://gitlab.com/ethima/semantic-release-configuration/commit/a18d67f5145c074ca2dc996eddf081ed85c5dc24)), closes [/gitlab.com/ethima/semantic-release/-/jobs/4661790403#L42](https://gitlab.com//gitlab.com/ethima/semantic-release/-/jobs/4661790403/issues/L42)

# [4.1.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v4.0.1...v4.1.0) (2023-07-06)


### Features

* **branches:** facilitate maintenance and prerelease branches ([1f3b971](https://gitlab.com/ethima/semantic-release-configuration/commit/1f3b971c0a2b6e46a37ecf5d7d86d3f50de48974)), closes [#5](https://gitlab.com/ethima/semantic-release-configuration/issues/5)

## [4.0.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v4.0.0...v4.0.1) (2023-06-14)


### Bug Fixes

* base platform detection on platform specific environment variables ([e1f75ad](https://gitlab.com/ethima/semantic-release-configuration/commit/e1f75ad885c41f830238c7153f2d4d08bed78df1)), closes [/github.com/semantic-release/gitlab/blob/b65f4ccea726ec2e556b1f9d3fe725118b1a9150/lib/verify.js#L31](https://gitlab.com//github.com/semantic-release/gitlab/blob/b65f4ccea726ec2e556b1f9d3fe725118b1a9150/lib/verify.js/issues/L31) [/github.com/semantic-release/github/blob/b8af600e064d104a1dc40129076a2eb3edab2bb1/lib/verify.js#L53](https://gitlab.com//github.com/semantic-release/github/blob/b8af600e064d104a1dc40129076a2eb3edab2bb1/lib/verify.js/issues/L53)

# [4.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.3.5...v4.0.0) (2023-04-25)


* feat!(versioned-templates): derive token configuration from filenames ([afb6bce](https://gitlab.com/ethima/semantic-release-configuration/commit/afb6bcee040a9def868c5f3068fe8a04b251f832))


### Features

* support versioned templates in C files ([b447cba](https://gitlab.com/ethima/semantic-release-configuration/commit/b447cba38401094646d4513ef508ded9c0b99938))
* support versioned templates in CSS files ([4f3b1a5](https://gitlab.com/ethima/semantic-release-configuration/commit/4f3b1a513fe802060e66ae6790d4b77e434396f4))
* support versioned templates in HTML files ([fe0e8b1](https://gitlab.com/ethima/semantic-release-configuration/commit/fe0e8b159a6caf11d78b93ab7c03b25f23ad0b0f))
* support versioned templates in JavaScript/TypeScript files ([0a8b115](https://gitlab.com/ethima/semantic-release-configuration/commit/0a8b115f6688ace8e5021e9d159a433241638cbf))
* support versioned templates in Julia files ([f09cfb8](https://gitlab.com/ethima/semantic-release-configuration/commit/f09cfb899922587d3c31f54f3aa7b02e127d1039))
* support versioned templates in Python files ([a76cf2d](https://gitlab.com/ethima/semantic-release-configuration/commit/a76cf2dc21c7f2d93a8b3ae0382a9417ec5db018))
* support versioned templates in TOML files ([2dcdb0a](https://gitlab.com/ethima/semantic-release-configuration/commit/2dcdb0a0463e385e6fc9235ef281a83fcc952aea))
* support versioned templates in XML files ([3cf94af](https://gitlab.com/ethima/semantic-release-configuration/commit/3cf94af1dc9076e105abd439239c82aa452ed4fb))
* support versioned templates in YAML files ([d6f631a](https://gitlab.com/ethima/semantic-release-configuration/commit/d6f631a98b0029b6f22f428246f0893e0a02be65))


### BREAKING CHANGES

* The previously used `<!-- START_VERSIONED_TEMPLATE` and
`<!-- END_VERSIONED_TEMPLATE -->` tokens are replaced with the
`BEGIN_VERSIONED_TEMPLATE` and `END_VERSIONED_TEMPLATE_REPLACEMENT`
tokens respectively. The `END_VERSIONED_TEMPLATE` token now indicates
the end of the template, not the end of the replaced content.

In practice this means that any templates should replace
`START_VERSIONED_TEMPLATE` with `BEGIN_VERSIONED_TEMPLATE` and
`END_VERSIONED_TEMPLATE` with `END_VERSIONED_TEMPLATE_REPLACEMENT`. All
other content in the template can stay the same, including comment
markers that were previously considered to be part of the tokens. These
markers are now considered as "additional content", but no longer
strictly matched against.

## [3.3.5](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.3.4...v3.3.5) (2023-04-25)


### Bug Fixes

* **deps:** update dependency cosmiconfig to v8.1.3 ([a6a48b5](https://gitlab.com/ethima/semantic-release-configuration/commit/a6a48b57d0e28390f4fe8fc386de7220c66485d0))

## [3.3.4](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.3.3...v3.3.4) (2023-04-25)


### Bug Fixes

* **deps:** update semantic-release monorepo ([84bd7a8](https://gitlab.com/ethima/semantic-release-configuration/commit/84bd7a8579255d0855d7f47b583ac7f66cdaf36a))

## [3.3.3](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.3.2...v3.3.3) (2023-03-10)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12 ([e1e7ed8](https://gitlab.com/ethima/semantic-release-configuration/commit/e1e7ed89b183389fd7032cbb885a32d9c81b10ea))

## [3.3.2](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.3.1...v3.3.2) (2023-02-26)


### Bug Fixes

* **deps:** update dependency cosmiconfig to v8.1.0 ([bb8efa6](https://gitlab.com/ethima/semantic-release-configuration/commit/bb8efa6193348363f51ab4bf50d6ed85f449c4a4))

## [3.3.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.3.0...v3.3.1) (2023-02-16)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v11 ([7dba901](https://gitlab.com/ethima/semantic-release-configuration/commit/7dba901be7c0e40b6943aa394cee19b6bc3ef29b))

# [3.3.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.2.2...v3.3.0) (2023-02-14)


### Features

* **julia:** update the `version` field in `Project.toml` files ([59e88c0](https://gitlab.com/ethima/semantic-release-configuration/commit/59e88c08907b14f65bae353a2c4c3da8d86548ce)), closes [#3](https://gitlab.com/ethima/semantic-release-configuration/issues/3)

## [3.2.2](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.2.1...v3.2.2) (2023-02-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10 ([8fd7764](https://gitlab.com/ethima/semantic-release-configuration/commit/8fd77646e1afa4c878d6de130b198fce4a9a25c6))

## [3.2.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.2.0...v3.2.1) (2023-01-19)


### Bug Fixes

* correct plugin names for platform-specific release plugins ([7ac4b79](https://gitlab.com/ethima/semantic-release-configuration/commit/7ac4b7965bddb793e87d65a78e80ccfcc67c0e69))

# [3.2.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.1.0...v3.2.0) (2023-01-18)


### Features

* enable configuration of the "primary release branch" ([7d097de](https://gitlab.com/ethima/semantic-release-configuration/commit/7d097de0fec0fba9ca19e9315c274a1bc8abf9ee))
* include hosting platform release plugins based on secret variables ([8132fac](https://gitlab.com/ethima/semantic-release-configuration/commit/8132faca5c44f0de99eabd9bcd254bded7414907))

# [3.1.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v3.0.0...v3.1.0) (2023-01-17)


### Features

* release to GitHub when running on GitHub Actions ([f82962a](https://gitlab.com/ethima/semantic-release-configuration/commit/f82962aac9f45179260e34863a9837eadfeb896f))

# [3.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v2.1.1...v3.0.0) (2023-01-17)


* feat!: switch to the `conventionalcommits` preset ([0310572](https://gitlab.com/ethima/semantic-release-configuration/commit/03105722577e120e453b636f97d589bf6fbac00b))


### BREAKING CHANGES

* The `conventionalcommits` preset is based on the
`angular` preset and hence largely similar. The primary difference is
that It also allows the use of a `!` suffix to type/scope of a commit to
indicate a breaking change. This is considered a breaking change as it
is a change in the default value and hence behavior of the
configuration.

## [2.1.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v2.1.0...v2.1.1) (2023-01-06)


### Bug Fixes

- **configuration:** guard against unavailable project-specific configuration ([4ac489e](https://gitlab.com/ethima/semantic-release-configuration/commit/4ac489e66e0ab78c032fe85033711f903c1e869b)), closes [#11](https://gitlab.com/ethima/semantic-release-configuration/issues/11)

# [2.1.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v2.0.0...v2.1.0) (2023-01-05)


### Features

* **configuration:** enable configuration of files with versioned templates ([03935f1](https://gitlab.com/ethima/semantic-release-configuration/commit/03935f1c37661fe98bfbec99346bef1f8b76dedf))
* **configuration:** enable configuration of the changelog filename ([51ef2a3](https://gitlab.com/ethima/semantic-release-configuration/commit/51ef2a3b8e93f1329318d1c756fda41c48eee79b))

# [2.0.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v1.1.1...v2.0.0) (2022-12-29)


* feat!: rename to `@ethima/semantic-release-configuration` ([587d2ba](https://gitlab.com/ethima/semantic-release-configuration/commit/587d2baef7e8cabcc83901b9058a7ffbb31f076a))


### BREAKING CHANGES

* The package has been renamed to
`@ethima/semantic-release-configuration`. The previous
`@bauglir/semantic-release-configuration` package is still available,
but is no longer intended to receive updates. The previous package name
may be reused for a personal semantic release configuration.

## [1.1.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v1.1.0...v1.1.1) (2022-12-22)

### Bug Fixes

- include `versioned-templates.js` in release tarballs ([b9a7341](https://gitlab.com/ethima/semantic-release-configuration/commit/b9a7341aece85433fb545b8b1af1b82ce91c7307))

# [1.1.0](https://gitlab.com/ethima/semantic-release-configuration/compare/v1.0.1...v1.1.0) (2022-12-22)

### Features

- update version references in root-level `README` files ([5b0e192](https://gitlab.com/ethima/semantic-release-configuration/commit/5b0e192eb508f549a0a4412555840a1e1b70a075))

## [1.0.1](https://gitlab.com/ethima/semantic-release-configuration/compare/v1.0.0...v1.0.1) (2022-12-21)

### Bug Fixes

- **deps:** update semantic-release monorepo ([3416047](https://gitlab.com/ethima/semantic-release-configuration/commit/341604735941a597fc6f457ce5d033975d53e080))

# 1.0.0 (2022-11-10)

### Bug Fixes

- **deps:** add `@semantic-release/{changelog,git}` plugins ([ec79c78](https://gitlab.com/ethima/semantic-release-configuration/commit/ec79c780c76fe58ce97c3a0cd79a202f33c7ff51))

### Features

- maintain a `CHANGELOG.md` file ([2cbc943](https://gitlab.com/ethima/semantic-release-configuration/commit/2cbc943ef5f33988ea944780249debab85e9ec02))
- **npm:** publish JavaScript projects to NPM ([97bc3bf](https://gitlab.com/ethima/semantic-release-configuration/commit/97bc3bf1714049338bbccd29f56ea689bb8259ba)), closes [#1](https://gitlab.com/ethima/semantic-release-configuration/issues/1)
- publish releases to GitLab from the default branch ([3fc9df0](https://gitlab.com/ethima/semantic-release-configuration/commit/3fc9df04e7badd03d05df71bfaf9d65efcccf22f))
